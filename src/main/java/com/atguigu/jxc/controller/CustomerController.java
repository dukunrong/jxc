package com.atguigu.jxc.controller;

import com.alibaba.fastjson2.JSON;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;


@RestController
@RequestMapping(value = "/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;


    /**
     * 客户列表分页（名称模糊查询）
     *
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    @PostMapping(value = "/list")
    public String listCustomer(Integer page, Integer rows, String customerName) {
        Map<String, Object> map = customerService.listCustomer(page, rows, customerName);
        return JSON.toJSONString(map);
    }


    /**
     * 客户添加或修改
     *
     * @param customer
     * @return
     */
    @PostMapping(value = "/save")
    public String saveCustomer(Customer customer) {
        customerService.save(customer);
        return JSON.toJSONString(new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS));
    }


    /**
     * 客户删除（支持批量删除）
     *
     * @param ids
     * @return
     */
    @PostMapping(value = "/delete")
    public String deleteCustomer(String ids) {
        customerService.delete(ids);
        return JSON.toJSONString(new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS));
    }

}
