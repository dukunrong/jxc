package com.atguigu.jxc.controller;

import com.alibaba.fastjson2.JSON;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.service.DamageListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping(value = "/damageListGoods")
public class DamageListGoodsController {

    @Autowired
    private DamageListGoodsService damageListGoodsService;


    /**
     * 保存报损单
     *
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    @PostMapping(value = "/save")
    public String saveDamageListGoods(DamageList damageList, String damageListGoodsStr) {
        damageListGoodsService.saveDamageListGoods(damageList, damageListGoodsStr);
        return JSON.toJSONString(new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS));
    }


    /**
     * 报损单查询
     *
     * @param sTime
     * @param eTime
     * @return
     */
    @PostMapping(value = "/list")
    public String listDamageListGoods(String sTime, String eTime) {
        Map<String, Object> map = damageListGoodsService.listDamageListGoods(sTime, eTime);
        return JSON.toJSONString(map);
    }


    /**
     * 报损单商品信息
     *
     * @param damageListId
     * @return
     */
    @PostMapping(value = "/goodsList")
    public String goodsList(Integer damageListId) {
        Map<String, Object> map = damageListGoodsService.goodsList(damageListId);
        return JSON.toJSONString(map);
    }

}
