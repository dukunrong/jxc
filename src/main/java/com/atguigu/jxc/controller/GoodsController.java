package com.atguigu.jxc.controller;

import com.alibaba.fastjson2.JSON;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.GoodsService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @description 商品信息Controller
 */

@RestController
@RequestMapping(value = "/goods")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;


    /**
     * 分页查询商品库存信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param codeOrName  商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping(value = "/listInventory")
    public String listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        Map<String, Object> map = goodsService.listInventory(page, rows, codeOrName, goodsTypeId);
        return JSON.toJSONString(map);
    }


    /**
     * 分页查询商品信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param goodsName   商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping(value = "/list")
    public String listGoods(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        Map<String, Object> map = goodsService.listGoods(page, rows, goodsName, goodsTypeId);
        return JSON.toJSONString(map);
    }


    /**
     * 生成商品编码
     *
     * @return
     */
    @RequestMapping("/getCode")
    @RequiresPermissions(value = "商品管理")
    public ServiceVO getCode() {
        return goodsService.getCode();
    }


    /**
     * 添加或修改商品信息
     *
     * @param goods 商品信息实体
     * @return
     */
    @PostMapping("/save")
    public String saveGoods(Goods goods) {
        goodsService.saveGoods(goods);
        return JSON.toJSONString(new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS));
    }


    /**
     * 删除商品信息
     *
     * @param goodsId 商品ID
     * @return
     */
    @PostMapping(value = "delete")
    public String deleteGoods(Integer goodsId) {
        ServiceVO serviceVO = goodsService.deleteGoods(goodsId);
        return JSON.toJSONString(serviceVO);
    }


    /**
     * 分页查询无库存商品信息
     *
     * @param page       当前页
     * @param rows       每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping(value = "/getNoInventoryQuantity")
    public String getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String, Object> map = goodsService.getNoInventoryQuantity(page, rows, nameOrCode);
        return JSON.toJSONString(map);
    }


    /**
     * 分页查询有库存商品信息
     *
     * @param page       当前页
     * @param rows       每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping(value = "/getHasInventoryQuantity")
    public String getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String, Object> map = goodsService.getHasInventoryQuantity(page, rows, nameOrCode);
        return JSON.toJSONString(map);
    }


    /**
     * 添加商品期初库存
     *
     * @param goodsId           商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice   成本价
     * @return
     */
    @PostMapping(value = "/saveStock")
    public String saveStock(Integer goodsId, Integer inventoryQuantity, Double purchasingPrice) {
        goodsService.saveStock(goodsId, inventoryQuantity, purchasingPrice);
        return JSON.toJSONString(new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS));
    }


    /**
     * 删除商品库存
     *
     * @param goodsId 商品ID
     * @return
     */
    @PostMapping(value = "/deleteStock")
    public String deleteStock(Integer goodsId) {
        ServiceVO serviceVO = goodsService.deleteStock(goodsId);
        return JSON.toJSONString(serviceVO);
    }


    /**
     * 查询库存报警商品信息
     *
     * @return
     */
    @PostMapping(value = "/listAlarm")
    public String listAlarm() {
        Map<String, Object> map = goodsService.listAlarm();
        return JSON.toJSONString(map);
    }

}
