package com.atguigu.jxc.controller;

import com.alibaba.fastjson2.JSON;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.service.OverflowListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping(value = "/overflowListGoods")
public class OverflowListGoodsController {

    @Autowired
    private OverflowListGoodsService overflowListGoodsService;

    /**
     * 新增报溢单
     *
     * @param overflowList
     * @param overflowListGoodsStr
     * @return
     */
    @PostMapping(value = "/save")
    public String saveOverflowListGoods(OverflowList overflowList, String overflowListGoodsStr) {
        overflowListGoodsService.saveOverflowListGoods(overflowList, overflowListGoodsStr);
        return JSON.toJSONString(new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS));

    }

    /**
     * 报溢单查询
     *
     * @param sTime
     * @param eTime
     * @return
     */
    @PostMapping(value = "/list")
    public String listOverflowListGoods(String sTime, String eTime) {
        Map<String, Object> map = overflowListGoodsService.listOverflowListGoods(sTime, eTime);
        return JSON.toJSONString(map);
    }

    /**
     * 报溢单商品信息
     *
     * @param overflowListId
     * @return
     */
    @PostMapping(value = "/goodsList")
    public String goodsList(Integer overflowListId) {
        Map<String, Object> map = overflowListGoodsService.goodsList(overflowListId);
        return JSON.toJSONString(map);
    }
}
