package com.atguigu.jxc.controller;

import com.alibaba.fastjson2.JSON;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping(value = "/supplier")
public class SupplierController {

    @Autowired
    private SupplierService supplierService;

    /**
     * 分页查询供应商
     *
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    @PostMapping(value = "/list")
    public String listSupplier(Integer page, Integer rows, String supplierName) {
        Map<String, Object> map = supplierService.listSupplier(page, rows, supplierName);
        return JSON.toJSONString(map);
    }

    /**
     * 供应商添加或修改
     *
     * @param supplier
     * @return
     */
    @PostMapping(value = "/save")
    public String saveSupplier(Supplier supplier) {
        supplierService.save(supplier);
        return JSON.toJSONString(new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS));
    }

    /**
     * 删除供应商（支持批量删除）
     *
     * @param ids
     * @return
     */
    @PostMapping(value = "/delete")
    public String deleteSupplier(String ids) {
        supplierService.delete(ids);
        return JSON.toJSONString(new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS));
    }

}
