package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CustomerDao {
    List<Customer> getListCustomer(@Param("page") Integer newPage,
                                   @Param("rows") Integer rows,
                                   @Param("customerName") String customerName);

    Integer getTotal(@Param("customerName") String customerName);

    void saveCustomer(Customer customer);

    void updateCustomer(Customer customer);

    void deleteCustomer(@Param("ids") String ids);
}
