package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface DamageListDao {

    /**
     * 保存 商品报损 数据
     *
     * @param damageList
     */
    void saveDamageList(DamageList damageList);

    /**
     * 根据 报损单号 查询对应的 报损单列表
     *
     * @param damageNumber
     * @return
     */
    DamageList getDamageListByNum(@Param("damageNumber") String damageNumber);
}
