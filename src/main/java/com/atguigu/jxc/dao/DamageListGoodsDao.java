package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DamageListGoodsDao {

    // 保存报损单
    void saveBatch(@Param("list") List<DamageListGoods> damageListGoodsList);

    /**
     * 报损单查询
     *
     * @param sTime
     * @param eTime
     * @return
     */
    List<DamageList> listDamageListGoods(@Param("sTime") String sTime,
                                         @Param("eTime") String eTime);

    /**
     * 报损单商品信息
     *
     * @param damageListId
     * @return
     */
    List<DamageListGoods> goodsList(@Param("damageListId") Integer damageListId);


}
