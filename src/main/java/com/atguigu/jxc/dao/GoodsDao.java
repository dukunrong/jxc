package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();

    List<Goods> getListInventory(@Param("page") Integer page,
                                 @Param("rows") Integer rows,
                                 @Param("codeOrName") String codeOrName,
                                 @Param("goodsTypeId") Integer goodsTypeId);


    List<Goods> getListGoods(@Param("page") Integer newPage,
                             @Param("rows") Integer rows,
                             @Param("goodsName") String goodsName,
                             @Param("goodsTypeId") Integer goodsTypeId);

    Integer getGoodsTotal(@Param("goodsName") String goodsName,
                          @Param("goodsTypeId") Integer goodsTypeId);

    /**
     * 添加商品信息
     *
     * @param goods
     */
    void saveGoods(Goods goods);

    /**
     * 修改商品信息
     *
     * @param goods
     */
    void updateGoods(Goods goods);

    /**
     * 删除商品
     *
     * @param goodsId
     */
    void deleteGoods(@Param("goodsId") Integer goodsId);

    Goods getGoodsById(@Param("goodsId") Integer goodsId);

    // 分页查询无库存商品信息
    List<Goods> getNoInventoryQuantity(@Param("page") Integer newPage,
                                       @Param("rows") Integer rows,
                                       @Param("nameOrCode") String nameOrCode);

    // 分页查询有库存商品信息
    List<Goods> getHasInventoryQuantity(@Param("page") Integer newPage,
                                        @Param("rows") Integer rows,
                                        @Param("nameOrCode") String nameOrCode);


    /**
     * 添加商品期初库存
     *
     * @param goodsId
     * @param inventoryQuantity
     * @param purchasingPrice
     */
    void saveStock(@Param("goodsId") Integer goodsId,
                   @Param("inventoryQuantity") Integer inventoryQuantity,
                   @Param("purchasingPrice") Double purchasingPrice);

    /**
     * 删除商品库存
     *
     * @param goodsId
     */
    void deleteStock(@Param("goodsId") Integer goodsId);

    /**
     * 查询库存报警商品信息
     *
     * @return
     */
    List<Goods> listAlarm();

}
