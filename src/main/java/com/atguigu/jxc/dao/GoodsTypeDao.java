package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);

    /**
     * 新增分类
     *
     * @param goodsTypeName
     * @param pId
     */
    void saveGoodsType(@Param("goodsTypeName") String goodsTypeName,
                       @Param("pId") Integer pId);

    GoodsType getGoodsTypeById(@Param("pId") Integer pId);

    /**
     * 删除分类
     *
     * @param goodsTypeId
     */
    void deleteGoodsType(@Param("goodsTypeId") Integer goodsTypeId);


}
