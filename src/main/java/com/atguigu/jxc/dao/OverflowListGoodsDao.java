package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OverflowListGoodsDao {

    /**
     * 新增报溢单
     *
     * @param overflowListGoodsList
     */
    void saveBatch(@Param("list") List<OverflowListGoods> overflowListGoodsList);

    /**
     * 报溢单查询
     *
     * @param sTime
     * @param eTime
     * @return
     */
    List<OverflowList> listOverflowListGoods(@Param("sTime") String sTime,
                                             @Param("eTime") String eTime);

    /**
     * 报溢单商品信息
     *
     * @param overflowListId
     * @return
     */
    List<OverflowListGoods> goodsList(@Param("overflowListId") Integer overflowListId);


}
