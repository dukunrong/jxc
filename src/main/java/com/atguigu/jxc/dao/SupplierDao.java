package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SupplierDao {
    List<Supplier> getListSupplier(@Param("page") Integer newPage,
                                   @Param("rows") Integer rows,
                                   @Param("supplierName") String supplierName);

    Integer getTotal(@Param("supplierName") String supplierName);

    void saveSupplier(Supplier supplier);

    void updateSupplier(Supplier supplier);

    void deleteSupplier(@Param("ids") String ids);
}
