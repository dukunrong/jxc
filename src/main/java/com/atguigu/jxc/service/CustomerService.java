package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Customer;

import java.util.Map;

public interface CustomerService {
    /**
     * 客户列表分页（名称模糊查询）
     *
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    Map<String, Object> listCustomer(Integer page, Integer rows, String customerName);

    /**
     * 客户添加或修改
     *
     * @param customer
     */
    void save(Customer customer);

    /**
     * 客户删除（支持批量删除）
     *
     * @param ids
     */
    void delete(String ids);
}
