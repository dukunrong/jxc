package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageList;

public interface DamageListService {

    /**
     * 保存 DamageList报损单 数据
     *
     * @param damageList
     */
    void saveDamageList(DamageList damageList);

    /**
     * 根据报损单号查询报损单
     *
     * @param damageNumber
     * @return
     */
    DamageList getDamageList(String damageNumber);
}
