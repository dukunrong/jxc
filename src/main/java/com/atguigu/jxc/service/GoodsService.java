package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;

import java.util.Map;

public interface GoodsService {


    ServiceVO getCode();


    /**
     * 查询当前库存（可根据商品类别、商品编码或名称搜索）
     *
     * @param page
     * @param rows
     * @param codeOrName
     * @param goodsTypeId
     * @return
     */
    Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);

    /**
     * 分页查询商品信息
     *
     * @param page
     * @param rows
     * @param goodsName
     * @param goodsTypeId
     * @return
     */
    Map<String, Object> listGoods(Integer page, Integer rows, String goodsName, Integer goodsTypeId);

    /**
     * 添加或修改商品信息
     *
     * @param goods
     */
    void saveGoods(Goods goods);

    /**
     * 删除商品
     *
     * @param goodsId
     */
    ServiceVO deleteGoods(Integer goodsId);

    /**
     * 分页查询无库存商品信息
     *
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    /**
     * 分页查询有库存商品信息
     *
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    /**
     * 添加商品期初库存
     *
     * @param goodsId
     * @param inventoryQuantity
     * @param purchasingPrice
     */
    void saveStock(Integer goodsId, Integer inventoryQuantity, Double purchasingPrice);

    /**
     * 删除商品库存
     *
     * @param goodsId
     */
    ServiceVO deleteStock(Integer goodsId);

    /**
     * 查询库存报警商品信息
     *
     * @return
     */
    Map<String, Object> listAlarm();
}
