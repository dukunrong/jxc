package com.atguigu.jxc.service;

import java.util.ArrayList;

/**
 * @description
 */
public interface GoodsTypeService {
    ArrayList<Object> loadGoodsType();

    /**
     * 新增分类
     *
     * @param goodsTypeName
     * @param pId
     */
    void saveGoodsType(String goodsTypeName, Integer pId);

    /**
     * 删除分类
     *
     * @param goodsTypeId
     */
    void deleteGoodsType(Integer goodsTypeId);
}
