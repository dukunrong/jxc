package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.OverflowList;

import java.util.Map;

public interface OverflowListGoodsService {

    /**
     * 新增报溢单
     *
     * @param overflowList
     * @param overflowListGoodsStr
     */
    void saveOverflowListGoods(OverflowList overflowList, String overflowListGoodsStr);

    /**
     * 报溢单查询
     *
     * @param sTime
     * @param eTime
     * @return
     */
    Map<String, Object> listOverflowListGoods(String sTime, String eTime);

    /**
     * 报溢单商品信息
     *
     * @param overflowListId
     * @return
     */
    Map<String, Object> goodsList(Integer overflowListId);
}
