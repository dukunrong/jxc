package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.OverflowList;

public interface OverflowListService {

    /**
     * 保存 overflowList报溢单 数据
     *
     * @param overflowList
     */
    void saveOverflowList(OverflowList overflowList);

    /**
     * 根据 报溢单号 查询对应的 报溢单
     *
     * @param overflowNumber
     * @return
     */
    OverflowList getOverflowList(String overflowNumber);

}
