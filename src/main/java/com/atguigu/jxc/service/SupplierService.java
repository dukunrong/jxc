package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

public interface SupplierService {
    /**
     * 分页查询供应商
     *
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    Map<String, Object> listSupplier(Integer page, Integer rows, String supplierName);

    /**
     * 供应商添加或修改
     *
     * @param supplier
     */
    void save(Supplier supplier);

    /**
     * 删除供应商（支持批量删除）
     *
     * @param ids
     */
    void delete(String ids);
}
