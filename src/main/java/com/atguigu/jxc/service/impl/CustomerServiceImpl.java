package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;

    @Override
    public Map<String, Object> listCustomer(Integer page, Integer rows, String customerName) {

        // 声明一个返回条件
        HashMap<String, Object> map = new HashMap<>();

        // 计算分页条件
        page = page == 0 ? 1 : page;
        Integer newPage = (page - 1) * rows;

        // 根据条件查询客户列表
        List<Customer> list = customerDao.getListCustomer(newPage, rows, customerName);
        Integer total = customerDao.getTotal(customerName);

        // 封装查询条件
        map.put("total", total);
        map.put("rows", list);

        // 返回
        return map;
    }

    @Override
    public void save(Customer customer) {
        if (customer.getCustomerId() == null) {
            log.info("添加客户");
            customerDao.saveCustomer(customer);
        } else {
            log.info("修改客户");
            customerDao.updateCustomer(customer);
        }
    }

    @Override
    public void delete(String ids) {
        customerDao.deleteCustomer(ids);
    }


}
