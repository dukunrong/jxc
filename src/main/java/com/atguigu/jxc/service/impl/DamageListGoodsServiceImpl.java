package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson2.JSON;
import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.service.DamageListGoodsService;
import com.atguigu.jxc.service.DamageListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DamageListGoodsServiceImpl implements DamageListGoodsService {

    @Autowired
    private DamageListGoodsDao damageListGoodsDao;
    @Autowired
    private DamageListService damageListService;

    /**
     * 保存报损单
     *
     * @param damageList
     * @param damageListGoodsStr
     */
    @Override
    public void saveDamageListGoods(DamageList damageList, String damageListGoodsStr) {

        // 把 damageList 数据保存到  t_damage_list 表中
        damageList.setUserId(1);
        damageListService.saveDamageList(damageList);

        //根据 报损单号 查询对应的 报损单
        DamageList damageListResult = damageListService.getDamageList(damageList.getDamageNumber());

        //把 damageListGoodsStr 数据保存到 t_damage_list_goods 表中
        List<DamageListGoods> damageListGoodsList = JSON.parseArray(damageListGoodsStr, DamageListGoods.class);

        //为 商品报损单详情表 设置 商品报损单id
        damageListGoodsList.stream().forEach(item -> item.setDamageListId(damageListResult.getDamageListId()));
        damageListGoodsDao.saveBatch(damageListGoodsList);

    }

    /**
     * 报损单查询
     *
     * @param sTime
     * @param eTime
     * @return
     */
    @Override
    public Map<String, Object> listDamageListGoods(String sTime, String eTime) {

        // 声明一个返回条件
        HashMap<String, Object> map = new HashMap<>();

        // 根据条件查询报损单
        List<DamageList> list = damageListGoodsDao.listDamageListGoods(sTime, eTime);

        // 封装返回条件
        map.put("rows", list);

        // 返回
        return map;
    }

    /**
     * 报损单商品信息
     *
     * @param damageListId
     * @return
     */
    @Override
    public Map<String, Object> goodsList(Integer damageListId) {

        // 声明一个返回条件
        HashMap<String, Object> map = new HashMap<>();

        // 根据条件查询报损单
        List<DamageListGoods> list = damageListGoodsDao.goodsList(damageListId);

        // 封装返回条件
        map.put("rows", list);

        // 返回
        return map;
    }
}
