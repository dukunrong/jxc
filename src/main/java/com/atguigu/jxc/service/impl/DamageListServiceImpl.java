package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageListDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.service.DamageListService;
import com.atguigu.jxc.util.DateUtil;
import com.atguigu.jxc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class DamageListServiceImpl implements DamageListService {

    @Autowired
    private DamageListDao damageListDao;

    @Override
    public void saveDamageList(DamageList damageList) {
        //日期判空校验
        if (damageList.getDamageDate() == null) {
            damageList.setDamageDate(DateUtil.DateToString(new Date(), "yyyy-MM-dd"));
        }
        if (damageList != null) {
            damageListDao.saveDamageList(damageList);
        }
    }

    @Override
    public DamageList getDamageList(String damageNumber) {
        DamageList damageList = null;
        if (!StringUtil.isEmpty(damageNumber)) {
            damageList = damageListDao.getDamageListByNum(damageNumber);
        }
        return damageList;
    }
}
