package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.GoodsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Slf4j
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;



    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();
        log.info("添加分支测试");

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for (int i = 4; i > intCode.toString().length(); i--) {

            unitCode = "0" + unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    /**
     * 查询当前库存（可根据商品类别、商品编码或名称搜索）
     *
     * @param page
     * @param rows
     * @param codeOrName
     * @param goodsTypeId
     * @return
     */
    @Override
    public Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {

        // 声明一个返回条件
        HashMap<String, Object> map = new HashMap<>();

        // 计算分页条件
        page = page == 0 ? 1 : page;
        Integer newPage = (page - 1) * rows;

        // 根据条件查询库存列表
        List<Goods> list = goodsDao.getListInventory(newPage, rows, codeOrName, goodsTypeId);

        // 封装
        map.put("total", list.size());
        map.put("rows", list);

        // 返回
        return map;
    }

    /**
     * 分页查询商品信息
     *
     * @param page
     * @param rows
     * @param goodsName
     * @param goodsTypeId
     * @return
     */
    @Override
    public Map<String, Object> listGoods(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {

        // 声明一个返回条件
        HashMap<String, Object> map = new HashMap<>();

        // 计算分页条件
        page = page == 0 ? 1 : page;
        Integer newPage = (page - 1) * rows;

        //if (goodsTypeId == 1) {
        //    goodsTypeId = null;
        //}

        // 根据条件查询商品信息
        List<Goods> list = goodsDao.getListGoods(newPage, rows, goodsName, goodsTypeId);
        Integer total = goodsDao.getGoodsTotal(goodsName, goodsTypeId);

        // 封装查询条件
        map.put("total", total);
        map.put("rows", list);

        // 返回
        return map;
    }

    /**
     * 添加或修改商品信息
     *
     * @param goods
     */
    @Override
    public void saveGoods(Goods goods) {
        if (goods.getGoodsId() == null) {
            log.info("添加商品信息");
            goods.setInventoryQuantity(0);
            goods.setState(0);
            goodsDao.saveGoods(goods);
        } else {
            log.info("修改商品信息");
            goods.setInventoryQuantity(0);
            goods.setState(0);
            goodsDao.updateGoods(goods);
        }
    }

    /**
     * 删除商品
     *
     * @param goodsId
     */
    @Override
    public ServiceVO deleteGoods(Integer goodsId) {
        //根据id查询商品，如果商品状态为 入库、有进货和销售单据的 不能删除
        Goods goods = goodsDao.getGoodsById(goodsId);
        if (goods.getState() == 1) {
            return new ServiceVO(ErrorCode.STORED_ERROR_CODE, ErrorCode.STORED_ERROR_MESS);
        }
        if (goods.getState() == 2) {
            return new ServiceVO(ErrorCode.HAS_FORM_ERROR_CODE, ErrorCode.HAS_FORM_ERROR_MESS);
        }
        //正常删除
        goodsDao.deleteGoods(goodsId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * 分页查询无库存商品信息
     *
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {

        // 声明一个返回条件
        HashMap<String, Object> map = new HashMap<>();

        // 计算分页条件
        page = page == 0 ? 1 : page;
        Integer newPage = (page - 1) * rows;

        // 根据条件查询商品信息
        List<Goods> list = goodsDao.getNoInventoryQuantity(newPage, rows, nameOrCode);

        // 封装查询条件
        map.put("total", list.size());
        map.put("rows", list);

        // 返回
        return map;
    }

    /**
     * 分页查询有库存商品信息
     *
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {

        // 声明一个返回条件
        HashMap<String, Object> map = new HashMap<>();

        // 计算分页条件
        page = page == 0 ? 1 : page;
        Integer newPage = (page - 1) * rows;

        // 根据条件查询商品信息
        List<Goods> list = goodsDao.getHasInventoryQuantity(newPage, rows, nameOrCode);

        // 封装查询条件
        map.put("total", list.size());
        map.put("rows", list);

        // 返回
        return map;
    }

    /**
     * 添加商品期初库存
     *
     * @param goodsId
     * @param inventoryQuantity
     * @param purchasingPrice
     */
    @Override
    public void saveStock(Integer goodsId, Integer inventoryQuantity, Double purchasingPrice) {
        goodsDao.saveStock(goodsId, inventoryQuantity, purchasingPrice);
    }

    /**
     * 删除商品库存
     *
     * @param goodsId
     */
    @Override
    public ServiceVO deleteStock(Integer goodsId) {
        //根据id查询商品，如果商品状态为 入库、有进货和销售单据的 不能删除
        Goods goods = goodsDao.getGoodsById(goodsId);
        if (goods.getState() == 1) {
            return new ServiceVO(ErrorCode.STORED_ERROR_CODE, ErrorCode.STORED_ERROR_MESS);
        }
        if (goods.getState() == 2) {
            return new ServiceVO(ErrorCode.HAS_FORM_ERROR_CODE, ErrorCode.HAS_FORM_ERROR_MESS);
        }
        //正常删除
        goodsDao.deleteStock(goodsId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * 查询库存报警商品信息
     *
     * @return
     */
    @Override
    public Map<String, Object> listAlarm() {

        // 声明一个返回条件
        HashMap<String, Object> map = new HashMap<>();

        // 根据条件查询库存报警商品信息
        List<Goods> list = goodsDao.listAlarm();

        // 封装查询条件
        map.put("rows", list);

        // 返回
        return map;
    }
}
