package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson2.JSON;
import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.service.OverflowListGoodsService;
import com.atguigu.jxc.service.OverflowListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OverflowListGoodsServiceImpl implements OverflowListGoodsService {

    @Autowired
    private OverflowListGoodsDao overflowListGoodsDao;
    @Autowired
    private OverflowListService overflowListService;

    /**
     * 新增报溢单
     *
     * @param overflowList
     * @param overflowListGoodsStr
     */
    @Override
    public void saveOverflowListGoods(OverflowList overflowList, String overflowListGoodsStr) {

        // 把 overflowList 数据保存到  t_overflow_list 表中
        overflowList.setUserId(1);
        overflowListService.saveOverflowList(overflowList);

        // 根据 报溢单号 查询对应的 报溢单
        OverflowList overflowListResult = overflowListService.getOverflowList(overflowList.getOverflowNumber());

        // 把 overflowListGoodsStr 数据保存到 t_overflow_list_goods 表中
        List<OverflowListGoods> overflowListGoodsList = JSON.parseArray(overflowListGoodsStr, OverflowListGoods.class);

        // 为 商品报溢单详情表 设置 商品报溢单id
        overflowListGoodsList.stream().forEach(item -> item.setOverflowListId(overflowListResult.getOverflowListId()));
        overflowListGoodsDao.saveBatch(overflowListGoodsList);
    }

    /**
     * 报溢单查询
     *
     * @param sTime
     * @param eTime
     * @return
     */
    @Override
    public Map<String, Object> listOverflowListGoods(String sTime, String eTime) {

        // 声明一个返回条件
        HashMap<String, Object> map = new HashMap<>();

        // 根据条件查询报损单
        List<OverflowList> list = overflowListGoodsDao.listOverflowListGoods(sTime, eTime);

        // 封装返回条件
        map.put("rows", list);

        // 返回
        return map;
    }

    /**
     * 报溢单商品信息
     *
     * @param overflowListId
     * @return
     */
    @Override
    public Map<String, Object> goodsList(Integer overflowListId) {

        // 声明一个返回条件
        HashMap<String, Object> map = new HashMap<>();

        // 根据条件查询报损单
        List<OverflowListGoods> list = overflowListGoodsDao.goodsList(overflowListId);

        // 封装返回条件
        map.put("rows", list);

        // 返回
        return map;
    }
}
