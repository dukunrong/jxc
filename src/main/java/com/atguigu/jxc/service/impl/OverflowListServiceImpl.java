package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.OverflowListDao;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.service.OverflowListService;
import com.atguigu.jxc.util.DateUtil;
import com.atguigu.jxc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class OverflowListServiceImpl implements OverflowListService {

    @Autowired
    private OverflowListDao overflowListDao;

    /**
     * 保存 overflowList报溢单 数据
     *
     * @param overflowList
     */
    @Override
    public void saveOverflowList(OverflowList overflowList) {
        //日期判空校验
        if (overflowList.getOverflowDate() == null) {
            overflowList.setOverflowDate(DateUtil.DateToString(new Date(), "yyyy-MM-dd"));
        }
        if (overflowList != null) {
            overflowListDao.saveOverflowList(overflowList);
        }
    }

    /**
     * 根据 报溢单号 查询对应的 报溢单
     *
     * @param overflowNumber
     * @return
     */
    @Override
    public OverflowList getOverflowList(String overflowNumber) {
        OverflowList overflowList = null;
        if (!StringUtil.isEmpty(overflowNumber)) {
            overflowList = overflowListDao.getOverflowListByNum(overflowNumber);
        }
        return overflowList;
    }
}
