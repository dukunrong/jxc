package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierDao supplierDao;

    @Override
    public Map<String, Object> listSupplier(Integer page, Integer rows, String supplierName) {

        // 声明一个返回条件
        HashMap<String, Object> map = new HashMap<>();

        // 计算分页条件
        page = page == 0 ? 1 : page;
        Integer newPage = (page - 1) * rows;

        // 根据条件查询供应商列表
        List<Supplier> list = supplierDao.getListSupplier(newPage, rows, supplierName);
        Integer total = supplierDao.getTotal(supplierName);

        // 封装查询条件
        map.put("total", total);
        map.put("rows", list);

        // 返回
        return map;
    }

    @Override
    public void save(Supplier supplier) {
        if (supplier.getSupplierId() == null) {
            log.info("添加供应商");
            supplierDao.saveSupplier(supplier);
        } else {
            log.info("修改供应商");
            supplierDao.updateSupplier(supplier);
        }
    }

    @Override
    public void delete(String ids) {
        supplierDao.deleteSupplier(ids);
    }


}
