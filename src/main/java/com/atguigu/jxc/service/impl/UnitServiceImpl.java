package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.UnitDao;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UnitServiceImpl implements UnitService {

    @Autowired
    private UnitDao unitDao;

    /**
     * 查询所有商品单位
     * @return
     */
    @Override
    public Map<String, Object> listUnit() {

        // 声明一个返回条件
        HashMap<String, Object> map = new HashMap<>();

        // 根据条件查询所有商品单位
        List<Unit> list = unitDao.getListUnit();

        // 封装查询条件
        map.put("rows",list);

        // 返回
        return map;
    }
}
